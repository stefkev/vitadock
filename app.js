var express = require("express"),
    bodyParser = require("body-parser"),
    cookieParser = require("cookie-parser"),
    session = require("express-session"),
    libLogin = require("./lib/login"),
    LibGetData = require("./lib/dataRetrieval"),
    modules = require('./modules');

var app = express();

app.set('env','dev');
//app.set('env','dist');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({secret: "sirakov-vitadock"}));


app.get('/', function (req, res) {
    res.end();
})
app.get('/notify', function (req, res) {
    res.end()
})
app.get('/callback', function (req, res, next) {
    libLogin.finalLogin(req, res, next);
})
app.get('/login', function (req, res, next) {
    libLogin.unauthorizedLogin(req, res, next);
})
app.get('/data', function (req, res) {
    res.end("you are logged in!");
//    var oauth_token = req.session.authorized_oauth_token;
//    var oauth_token_secret = req.session.authorized_oauth_token_secret;
//    console.log(oauth_token);
//    console.log(oauth_token_secret);
//    var authHeader = oauthAssembly.moduleStats(modules.cardioDock.module_name, oauth_token,
//        oauth_token_secret);
//    request.get(mainUrl + '/data/' + modules.cardioDock.module_name, {json: true, headers: {
//        'Authorization': authHeader
//    }},function(err, resp, body){
//        res.send(body);
//    })
})
app.get('/getdata/:model',function(req, res, next){
    console.log(req.params.model)
    LibGetData.getData(req, res, next, req.params.model)
})


app.listen(3005);

