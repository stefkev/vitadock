var request = require("request"),
    oauthAssembly = require("./oauthAssembly"),
    stringHelper = require("./stringHelper"),
    qs = require('querystring');

//if (process.env.NODE_ENV == 'dev'){
//    var mainUrl = "https://vitacloud.medisanaspace.com";
//}
//if (process.NODE_ENV == 'dist'){
//    var mainUrl;
//}
//var mainUrl = "https://vitacloud.medisanaspace.com";
var mainUrl = "https://cloud.vitadock.com";

module.exports.unauthorizedLogin = function(req,res, next){
    getUnauthhorizedKeys(function(unauthorizedKeys){
        req.session.unauthorized_oauth_token = unauthorizedKeys.oauth_token;
        req.session.unauthorized_oauth_token_secret = unauthorizedKeys.oauth_token_secret;
        next(res.redirect(mainUrl+ '/desiredaccessrights/request?oauth_token=' + unauthorizedKeys.oauth_token));
    })
}
module.exports.finalLogin = function(req, res, next){
    var authorizedKeys = req.query;
    console.log(authorizedKeys)
    req.session.oauth_token = authorizedKeys.oauth_token;
    req.session.oauth_verifier = authorizedKeys.oauth_verifier;
    getAuhtorizedKeys(req.session.oauth_token, req.session.oauth_verifier, req.session.unauthorized_oauth_token_secret,function(authorizedKeys){
        req.session.authorized_oauth_token = authorizedKeys.oauth_token;
        req.session.authorized_oauth_token_secret = authorizedKeys.oauth_token_secret;
        console.log(req.session.authorized_oauth_token);
        console.log(req.session.authorized_oauth_token_secret);
        next(res.redirect('/data'));
    })
}

function getUnauthhorizedKeys(callback){
    var date = stringHelper.date();
    var nonce = stringHelper.nonce();
    oauthAssembly.baseParameter(date,nonce,null,null, function(baseParameterData){
        oauthAssembly.requestUrl(mainUrl+'/auth/unauthorizedaccesses',function(url){
            oauthAssembly.signatureBaseString("POST",url,baseParameterData,function(signatureBaseString){
                oauthAssembly.signature(signatureBaseString,null, function(singature){
                    oauthAssembly.authorizationHeader(date, nonce, singature, null, null, function(authHeader){
                        request.post(mainUrl+'/auth/unauthorizedaccesses',{
                            headers:{
                                'Content-type':'application/x-www-form-urlencoded',
                                'Authorization': authHeader
                            }
                        },function(err, resp, body){
                            if (err){
                                callback(err);
                            }
                            var keys = qs.parse(body);
                            callback({oauth_token: keys.oauth_token, oauth_token_secret: keys.oauth_token_secret})
                        });
                    })
                })
            })
        })

    })
}

function getAuhtorizedKeys(access_token, verifier_token, access_token_secret,callback){
    var nonce = stringHelper.nonce();
    var date = stringHelper.date();
    oauthAssembly.baseParameter(date, nonce,access_token, verifier_token, function(baseParameterData){
        oauthAssembly.requestUrl(mainUrl+'/auth/accesses/verify',function(url){
            oauthAssembly.signatureBaseString('POST',url, baseParameterData, function(signatureBaseString){
                oauthAssembly.signature(signatureBaseString, access_token_secret, function(signature){
                    oauthAssembly.authorizationHeader(date, nonce, signature, access_token, verifier_token, function(authHeader){
                        request.post(mainUrl+'/auth/accesses/verify' ,{headers:{
                            'Content-type': 'application/x-www-form-urlencoded',
                            'Authorization': authHeader
                        }}, function(err, resp, body){
                            var finalKeys = qs.parse(body);
                            callback({oauth_token: finalKeys.oauth_token, oauth_token_secret: finalKeys.oauth_token_secret})
                        })
                    })
                })
            })
        })
    })
}
