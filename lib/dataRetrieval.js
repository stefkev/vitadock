var request = require("request"),
    oauthAssembly = require("./oauthAssembly"),
    stringHelper = require("./stringHelper"),
    qs = require('querystring');

//var mainUrl = "https://vitacloud.medisanaspace.com";
//var mainUrl = "https://vitacloud.medisanaspace.com"
var mainUrl = "https://cloud.vitadock.com";


module.exports.getData = function(req, res, next, module_name){
    getData(req.session.authorized_oauth_token, req.session.authorized_oauth_token_secret, module_name, function(data){
        res.json(data);
    })
}
function getData(access_token, access_token_secret,module_name, callback){
    var nonce = stringHelper.nonce();
    var date = stringHelper.date();
    oauthAssembly.baseParameter(date, nonce,access_token, null, function(baseParameterData){
        console.log(baseParameterData);
        oauthAssembly.requestUrl(mainUrl+'/data/'+module_name+'/sync?start=1&max=100&date_since=0', function(url){
            console.log(url)
            oauthAssembly.signatureBaseString('GET',url, baseParameterData, function(signatureBaseString){
                console.log(signatureBaseString);
                oauthAssembly.signature(signatureBaseString, access_token_secret, function(signature){
                    console.log(signature)
                    oauthAssembly.authorizationHeader(date, nonce, signature, access_token, null, function(authHeader){
                        console.log(authHeader);
                        request.get(mainUrl+'/data/'+module_name+'/sync?start=1&max=100&date_since=0',{headers:{
                            'Accept' : "application/json",
                            'Content-Type': 'application/json;charset=utf-8',
                            'Authorization': authHeader
                        }},function(err, resp, body){
                            console.log("request err:"+err);
                            console.log("============");
                            console.log("body "+body);
                            console.log("============");
                            console.log("response status code "+resp.statusCode);
                            console.log("============");
                            callback(body);
                        });
                    })
                })
            })
        })
    })
}