var oauthVars = require("./oauthVars"),
    stringHelper = require("./stringHelper"),
    url = require("url");
//===========================================
//===========================================

//var keys = {
//    application_token: "huEPRNem4nMkTcFbwjrv31Sg2x22u9dLThAVlohVB5wA4ko9rjzdGKWiZ3ZRhDLe",
//    application_secret: "YNeVMuvF6vgs8ShKvKUuAlJjg5byRTbuUv2n42UU9JusGyp8CSLlsoWi1A4Bm33N"
//}


var keys = {
    application_token: "AXXdFxaNDZvpPV15FBZUFxcGgSAADs8T5XkRf20Rq3Em0Xyxh9wDaZ29cmN8ZeRJ",
    application_secret: "jYPK3TcSg0edX5yCec5RnaCBMgBMvAYyKjLbMgkNL5Tsn1FWeSw1gGaEhCCFgEfa"
}


module.exports.baseParameter = function baseParameterString(timestamp, nonce, oauth_token, oauth_verifier,callback) {
    var myBaseParameter = ""
    if (oauth_token === null && oauth_verifier === null) {
        myBaseParameter = oauthVars.vars.consumer_key + '=' + keys.application_token + '&' + oauthVars.vars.nonce + '=' + nonce
            + '&' + oauthVars.vars.signature_method + '=HMAC-SHA256' + '&' + oauthVars.vars.timestamp + '=' + timestamp +
            '&' + oauthVars.vars.version + '=1.0';
    }
    if (oauth_verifier === null && oauth_token !== null) {
        myBaseParameter = oauthVars.vars.date_since + '=0' + '&' + oauthVars.vars.max + '=100' + '&' + oauthVars.vars.consumer_key +
            '=' + keys.application_token + '&' + oauthVars.vars.nonce + '=' + nonce
            + '&' + oauthVars.vars.signature_method + '=HMAC-SHA256' + '&' + oauthVars.vars.timestamp + '=' + timestamp +
            '&' + oauthVars.vars.token + '=' + oauth_token + '&' + oauthVars.vars.version + '=1.0'+ '&' +oauthVars.vars.start + '=1';
    }
    if (oauth_verifier !== null && oauth_token !== null) {
         myBaseParameter = oauthVars.vars.consumer_key + '=' + keys.application_token + '&' + oauthVars.vars.nonce + '=' + nonce
            + '&' + oauthVars.vars.signature_method + '=HMAC-SHA256' + '&' + oauthVars.vars.timestamp + '=' + timestamp +
            '&' + oauthVars.vars.token + '=' + oauth_token + '&' + oauthVars.vars.verifier + '=' + oauth_verifier + '&'
            + oauthVars.vars.version + '=1.0';
    }
    callback(myBaseParameter);
}
module.exports.signature = function signature(signatureBaseString, access_token_secret,cb) {
    var mysecret = '';
    if (access_token_secret === null) {
        mysecret = keys.application_secret + '&'
    } else {
        mysecret = keys.application_secret + '&' + access_token_secret;
    }
    var mySignature = stringHelper.hmac(mysecret, signatureBaseString)
    cb(mySignature);
}

module.exports.requestUrl = function requestUrl(myUrl, cb) {
    var parsedUrl = url.parse(myUrl);
    var formattedUrl = parsedUrl.protocol+"//"+parsedUrl.host+parsedUrl.pathname;
    var encodedUrl = stringHelper.myUrlEncoded(formattedUrl);
    console.log("formatted url "+formattedUrl);
    cb(encodedUrl);
}

module.exports.signatureBaseString = function signatureBaseString(method, encodedRequestUrl, baseParameterString,cb) {
    var mySignatureBaseString = method + "&" + encodedRequestUrl + "&" + stringHelper.myUrlEncoded(baseParameterString);
    cb(mySignatureBaseString);
}

module.exports.authorizationHeader = function authorizationHeader(date, nonce, signature, access_token, verifier, cb) {
    var myAuthorizationHeader = "";
    if (access_token === null && verifier === null) {
        myAuthorizationHeader = "OAuth " + oauthVars.vars.consumer_key + "=\"" + keys.application_token + "\"," + oauthVars.vars.nonce + "=\"" + nonce + "\","
            + oauthVars.vars.signature_method + "=\"HMAC-SHA256\"," + oauthVars.vars.timestamp + "=\"" + date + "\","
            + oauthVars.vars.version + "=\"1.0\"," + oauthVars.vars.signature + "=\"" + signature + "\"";
    }
    if (verifier === null && access_token !== null) {
        myAuthorizationHeader = "OAuth " + oauthVars.vars.consumer_key + "=\"" + keys.application_token + "\"," + oauthVars.vars.nonce + "=\"" + nonce + "\","
            + oauthVars.vars.signature_method + "=\"HMAC-SHA256\"," + oauthVars.vars.timestamp + "=\"" + date + "\","
            + oauthVars.vars.token + "=\"" + access_token + "\"," + oauthVars.vars.version + "=\"1.0\","
            + oauthVars.vars.signature + "=\"" + signature + "\"";
    }
    if (verifier != null && access_token != null) {
        myAuthorizationHeader = "OAuth " + oauthVars.vars.consumer_key + "=\"" + keys.application_token + "\"," + oauthVars.vars.nonce + "=\"" + nonce + "\","
            + oauthVars.vars.signature_method + "=\"HMAC-SHA256\"," + oauthVars.vars.timestamp + "=\"" + date + "\","
            + oauthVars.vars.token + "=\"" + access_token + "\"," + oauthVars.vars.verifier + "=\"" + verifier + "\","
            + oauthVars.vars.version + "=\"1.0\"," + oauthVars.vars.signature + "=\"" + signature + "\"";
    }
    cb(myAuthorizationHeader);
}
